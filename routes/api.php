<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/', function (Request $request) {
//    die();
//});

Route::group(['namespace' => 'Api'], function() {

    Route::get('handshake', function () {
        return '1';
    });

    // Lists
    Route::group(['prefix'=> 'lists', 'as' => 'lists.'], function () {

        Route::get('/',  ['as' => 'index', 'uses' => 'Audience\AudienceController@index'] );
        Route::post('/', ['as' => 'store', 'uses' => 'Audience\AudienceController@store']);
        Route::patch('/{id}', ['as' => 'update', 'uses' => 'Audience\AudienceController@update']);
        Route::get('/{id}',  ['as' => 'show', 'uses' => 'Audience\AudienceController@show'] );
        Route::delete('/{id}', ['as' => 'delete', 'uses' => 'Audience\AudienceController@delete']);

        // Members
        Route::get('/{listId}/members', ['as' => 'members.index', 'uses' => 'Member\MemberController@index'] );
        Route::post('/{listId}/members', ['as' => 'members.store', 'uses' => 'Member\MemberController@store'] );
        Route::get('/{listId}/members/{id}', ['as' => 'members.show', 'uses' => 'Member\MemberController@show'] );
        Route::patch('/{listId}/members/{id}', ['as' => 'members.update', 'uses' => 'Member\MemberController@update'] );
        Route::delete('/{listId}/members/{id}', ['as' => 'members.delete', 'uses' => 'Member\MemberController@delete'] );

    });
});