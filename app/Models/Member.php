<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;

class Member
{
    /**
     * Save members to cache
     *
     * @param string $listId
     * @param $data
     *
     * @return void
     */
    public static function saveMembers(string $listId, array $data)
    {
        Cache::put('mailchimp_list_' . $listId . 'members', $data, self::getCacheMinutes());
    }

    /**
     * Save member to cache
     *
     * @param string $listId
     * @param string $id
     * @param $data
     * @param bool $flashCache
     *
     * @return void
     */
    public static function save(string $listId, string $id, array $data, bool $flashCache = false)
    {
        if ($flashCache) {
            Cache::delete('mailchimp_lists');
            Cache::delete('mailchimp_list_' . $listId);
            Cache::delete('mailchimp_list_' . $listId . 'members');
        }
        Cache::put('mailchimp_list_' . $listId . '_member_' . $id, $data, self::getCacheMinutes());
    }

    /**
     * Delete member and flash data
     *
     * @param string $listId
     * @param string $id
     *
     * @return void
     */
    public static function delete(string $listId, string $id)
    {
        Cache::delete('mailchimp_lists');
        Cache::delete('mailchimp_list_' . $listId);
        Cache::delete('mailchimp_list_' . $listId . 'members');
        Cache::delete('mailchimp_list_' . $listId . '_member_' . $id);
    }

    public static function getCacheMinutes()
    {
        return env('MEMCACHED_MINUTES', 60);
    }
}