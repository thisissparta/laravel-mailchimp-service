<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;

class Audience
{
    /**
     * Save audiences to cache
     *
     * @param array $data
     *
     * @return void
     */
    public static function saveAudiences(array $data)
    {
        Cache::put('mailchimp_lists', $data, self::getCacheMinutes());
    }

    /**
     * Save audience to cache
     *
     * @param string $id
     * @param $data
     * @param bool $flashCache
     *
     * @return void
     */
    public static function save(string $id, array $data, bool $flashCache = false)
    {
        if ($flashCache) {
            Cache::delete('mailchimp_lists');
            Cache::delete('mailchimp_list_' . $id);
        }
        Cache::put('mailchimp_list_' . $id, $data, self::getCacheMinutes());
    }

    /**
     * Delete audience and flash data
     *
     * @param string $id
     *
     * @return void
     */
    public static function delete(string $id)
    {
        Cache::delete('mailchimp_lists');
        Cache::delete('mailchimp_list_' . $id);
        Cache::delete('mailchimp_list_' . $id . 'members');
    }

    public static function getCacheMinutes()
    {
        return env('MEMCACHED_MINUTES', 60);
    }
}