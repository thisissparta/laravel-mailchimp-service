<?php

namespace App\Http\Controllers\Api\Member;

use App\Exceptions\MailchimpException;
use Newsletter;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MemberController extends Controller
{
    /* @var \DrewM\MailChimp\MailChimp $mailchimp */
    public $mailchimp;

    public function __construct()
    {
        $this->mailchimp = Newsletter::getApi();
    }

    /**
     * Get all members from lists
     *
     * @param string $listId
     *
     * @return JsonResponse
     */
    public function index(string $listId)
    {
        $response = [];

        $data = $this->getMembers($listId);

        $response['data'] = [];

        Member::saveMembers($listId, $data);

        $response['data'] = $data;

        return response()->json($response);
    }

    /**
     * Show member from list
     *
     * @param string $listId
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $listId, string $id)
    {
        $response = [];

        $data = $this->getMember($listId, $id);

        $response['data'] = [];

        Member::save($listId, $id, $data);

        $response['data'] = $data;

        return response()->json($response);
    }

    /**
     * Create new member
     *
     * @param Request $request
     * @param string $listId
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function store(Request $request, string $listId)
    {
        $result = $this->mailchimp->post('lists/' . $listId . '/members', $request->all());

        $response = [];
        $response['data'] = [];

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        Member::save($listId, $result['id'], $result, true);

        $response['data'] = $result;

        return response()->json($response);
    }

    /**
     * Edit member
     *
     * @param Request $request
     * @param string $listId
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function update(Request $request, string $listId, string $id)
    {
        $result = $this->mailchimp->patch('lists/' . $listId . '/members/' . $id, $request->all());

        $response = [];
        $response['data'] = [];

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        Member::save($listId, $result['id'], $result, true);

        $response['data'] = $result;

        return response()->json($response);
    }

    /**
     * Delete member
     *
     * @param string $listId
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function delete(string $listId, string $id)
    {
        $result = $this->mailchimp->delete('lists/' . $listId . '/members/' . $id);

        $response = [];
        $response['data'] = [];

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        Member::delete($listId, $id);

        return response()->json($response);
    }

    /**
     * Get members from cache or do request to Mailchimp API
     *
     * @param string $listId
     *
     * @throws MailchimpException
     *
     * @return array|false|mixed
     */
    protected function getMembers(string $listId)
    {
        $cachedMembers = Cache::pull('mailchimp_list_' . $listId . '_members');

        if($cachedMembers) {
            return $cachedMembers;
        }

        $result = $this->mailchimp->get('lists/' . $listId . '/members');

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        return $result;
    }

    /**
     * Get member from cache or do request to Mailchimp API
     *
     * @param string $listId
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return array|false|mixed
     */
    protected function getMember(string $listId, string $id)
    {
        $cachedMembers = Cache::pull('mailchimp_list_' . $listId . '_member_' . $id);

        if($cachedMembers) {
            return $cachedMembers;
        }

        $result = $this->mailchimp->get('lists/' . $listId . '/members/' . $id);

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        return $result;
    }

}