<?php

namespace App\Http\Controllers\Api\Audience;

use App\Exceptions\MailchimpException;
use Newsletter;
use App\Models\Audience;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class AudienceController extends Controller
{
    /* @var \DrewM\MailChimp\MailChimp $mailchimp */
    public $mailchimp;

    public function __construct()
    {
        $this->mailchimp = Newsletter::getApi();
    }

    /**
     * Get all lists
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function index()
    {
        $response = [];

        $data = $this->getAudiences();

        $response['data'] = [];

        Audience::saveAudiences($data);

        $response['data'] = $data;

        return response()->json($response);
    }

    /**
     * Show list
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id)
    {
        $response = [];

        $data = $this->getAudience($id);

        $response['data'] = [];

        Audience::save($id, $data);

        $response['data'] = $data;

        return response()->json($response);
    }

    /**
     * Create new list
     *
     * @param Request $request
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $result = $this->mailchimp->post('lists', $request->all());

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        $response = [];
        $response['data'] = [];

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        Audience::save($result['id'], $result, true);

        $response['data'] = $result;

        return response()->json($response);
    }

    /**
     * Edit list
     *
     * @param Request $request
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function update(Request $request, string $id)
    {
        $result = $this->mailchimp->patch('lists/' . $id, $request->all());

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        $response = [];
        $response['data'] = [];

        Audience::save($id, $result, true);

        $response['data'] = $result;

        return response()->json($response);
    }

    /**
     * Delete list
     *
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return JsonResponse
     */
    public function delete(string $id)
    {
        $result = $this->mailchimp->delete('lists/' . $id);

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        $response = [];
        $response['data'] = [];

        Audience::delete($id);

        return response()->json($response);
    }

    /**
     * Get audiences from cache or do request to Mailchimp API
     *
     * @throws MailchimpException
     *
     * @return array|false|mixed
     */
    protected function getAudiences()
    {
        $cachedLists = Cache::pull('mailchimp_lists');

        if ($cachedLists) {
            return $cachedLists;
        }

        $result = $this->mailchimp->get('lists');

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        return $result;
    }


    /**
     * Get audience from cache or do request to Mailchimp API
     *
     * @param string $id
     *
     * @throws MailchimpException
     *
     * @return array|false|mixed
     */
    protected function getAudience(string $id)
    {
        $cachedList = Cache::pull('mailchimp_list_' . $id);

        if ($cachedList) {
            return $cachedList;
        }

        $result = $this->mailchimp->get('lists/' . $id);

        if ($this->mailchimp->getLastError()) {
            throw new MailchimpException($this->mailchimp->getLastError(), $result['status']);
        }

        return $result;
    }
}