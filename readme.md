
# Mailcimp Service based on Laravel 5.5  

### Service for manage your MailChimp Auditories and Subscribers  

## Full Installation

1. Go to your root project folder.  
  
2. Clone Docker files:  
  
    ```git clone  https://thisissparta@bitbucket.org/thisissparta/php7-1-laravel-docker.git .```
  
3. Copy docker-compose.yml.dist to docker-compose.yml  
  
    ```cp docker-compose.yml.dist docker-compose.yml``` 
  
4. Create website folder nginx/www/test-mailing.org folder  
  
    ```mkdir nginx/www/test-mailing.org```
  
5. Install Docker images and start containers*  
  
    ```docker-compose up -d```
  
6. Go to website folder:  
  
    ```cd nginx/www/test-mailing.org```
  
7. Clone website to nginx/www/test-mailing.org folder  
  
    ```git clone  https://thisissparta@bitbucket.org/thisissparta/laravel-mailchimp-service.git .```

  

8. Copy .env.example to .env

  

9. Set your Mailchimp API Key value in MAILCHIMP_APIKEY in .env  

  

    ```MAILCHIMP_APIKEY=```

  

10. Do compopser instal from PHP container  
  
    ```docker exec -ti php-fpm bash```
  
    ```cd /var/www/test-mailing.org```  

  
    ```composer install```  
  
11. Open yout host file and add following string:  
  
    ```127.0.0.1 test-mailing.org```  
  
**Installation complete**  
  
## Getting Started

You can see RESTful API requests in Postman Mailchimp.postman_collection.json file 


**The End**

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Pulse Storm](http://www.pulsestorm.net/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
